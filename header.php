<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gojira
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

	<header class="primary-header">
		<div class="primary-nav">
    	    <div class="primary-nav__wrapper container">
    	        <nav class="navbar navbar-default">
    	            <div class="container-fluid">
    	                <!-- Brand and toggle get grouped for better mobile display -->
    	                <div class="navbar-header">
    	                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
    	                        <span class="sr-only">Toggle navigation</span>
    	                        <span class="icon-bar"></span>
    	                        <span class="icon-bar"></span>
    	                        <span class="icon-bar"></span>
    	                    </button>
    	                    <a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="primary-nav__logo__link navbar-brand">
    	                        <picture class="primary-nav__logo__image">
    	                            <img srcset="https://placeholdit.imgix.net/~text?txtsize=13&txt=120%C3%9750&w=120&h=50" alt="Logo">
    	                        </picture>
    	                    </a>
    	                </div>
	
    	                <!-- Collect the nav links, forms, and other content for toggling -->
    	                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
    	                    <ul class="nav navbar-nav navbar-right">
                                <li class="primary-nav__item">
                                    <a data-scroll href="#item">Item</a>
                                </li>
                                <li class="primary-nav__item">
                                    <a data-scroll href="#item">Item</a>
                                </li>
                                <li class="primary-nav__item">
                                    <a data-scroll href="#item">Item</a>
                                </li>
                                <li class="primary-nav__item">
                                    <a data-scroll href="#item">Item</a>
                                </li>
                                <li class="primary-nav__item">
                                    <a data-scroll href="#item">Item</a>
                                </li>
                            </ul>
    	                </div><!-- /.navbar-collapse -->
    	            </div><!-- /.container-fluid -->
    	        </nav>
    	    </div>
    	</div>
    </header>

	<div id="content" class="site-content">
