<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package gojira
 */

get_header(); ?>


<?php get_template_part( 'blocks/block', 'header'); ?>

<?php get_template_part( 'blocks/block', 'news' ); ?>

<?php get_template_part( 'blocks/block', 'aftermovie' ); ?>

<?php get_template_part( 'blocks/block', 'lineup' ); ?>

<?php get_template_part( 'blocks/block', 'sponsor' ); ?>



<?php
//get_sidebar();
get_footer();
