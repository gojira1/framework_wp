# Wordpress base theme

## install

1. Download Wordpress from wordpress.org
2. Install Wordpress on your local computer
3. Git clone this repo in wp-content/themes folder

```
git@gitlab.com:gojira1/framework_wp.git
```

4. Open this theme folder in your terminal and install node modules

``` 
npm install
``` 

5. All the node modules are installed and you are ready to go.


## usage
1. Place all your css in src/styles/
2. Your scripts in src/scripts
3. Images in src/images
4. fonts in src/fonts

If you run gulp or gulp watch, all your files are being minified and copied to /dist/assets/

### Is it possible to add more folders in /src ?

Ofcourse!

1. Simply create a new folder in /src
2. Create a new gulp task, for example "docs"

``` 
gulp.task('docs', function() {
  return gulp.src('src/docs/**/*')
    .pipe(gulp.dest('dist/assets/docs'))
    .pipe(notify({ message: 'Docs task complete' }));
});
``` 

3. Add docs to gulp.task(watch)

```
  gulp.watch('src/docs/**/*', ['docs']);
``` 

4. That's it. Next time when you type "gulp" or "gulp watch" in your terminal, the docs folder is being copied to /dist/assets/


# Add functionalities

## 1. widgets

### 1.1 Create the initial widget

Add this code block in custom-functions.php

```
<?php
/**
 * Register our sidebars and widgetized areas.
 *
 */
function arphabet_widgets_init() {

	register_sidebar( array(
		'name'          => 'Home right sidebar',
		'id'            => 'home_right_1',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'arphabet_widgets_init' );
?>
```

### 1.2 Display your new widget

Add this code block somewhere in your theme to make the widgets available

```
<?php if ( is_active_sidebar( 'home_right_1' ) ) : ?>
	<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
		<?php dynamic_sidebar( 'home_right_1' ); ?>
	</div><!-- #primary-sidebar -->
<?php endif; ?>
```  


## 2. custom posttypes

### 2.1 Create the posttype

Add this code block in custom-functions.php
This will create a custom posttype

```
function create_posttype() {
    register_post_type( 'posttypename', // All in lowercase
      array(
        'labels' => array(
          'name' => __( 'Names' ), // Plural name of your new post type
          'singular_name' => __( 'Name' ) // Singular name of your new post type
        ),
        'public' => true,
        'has_archive' => true,
        'rewrite' => array('slug' => 'name'), // Singular and lowercase
      )
    );

    // copy here another custom posttype
}

add_action( 'init', 'create_posttype' );
``` 

In this template I used 'artist' and 'sponsor' as a custom post type. You can change or delete this by deleting the register_post_type in inc/custom-functions.php


### 2.2 Extend custom posttype or page/post

If you want to extend the Custom posttype, you can download:

Option Framework: https://nl.wordpress.org/plugins/options-framework/

or

Advanced Custom Fields: http://www.advancedcustomfields.com/

I prefer the ACF plugin. Also the easiest to implement.


## 3. Styles and scripts

You can find all the style and script includes in inc/custom-styles-scripts.php
It is important for Wordpress that you include all your files in this specific file.
Don't include CSS or JS files directly in your header or footer.


## 4. Pages 

To create a new page, you can simply duplicate or alter page-artist.php

1. alter the page name after the dash, so page-<name>.php
2. change the template name. This is on top of the page itself.

```
* Template Name: Page artist 
```

This is important that this stays a comment.

The template name is used in the Wordpress backend. If you create a new page, you can select a specific template.


### 4.1 custom loop on a page

```
<?php
  $args = array( 'post_type' => 'artist');
  $loop = new WP_Query( $args );
    
  while ( $loop->have_posts() ) : $loop->the_post();
?>

  <?php the_title(); ?>

<?php
  endwhile;
?>
```

The loop above shows all the artists. But it's also possible to extend the arguments of the array.


### 4.2 detail page of custom type.

So you've created a new custom type artist. This artist also has to have a detail page.
The default detail page of a Wordpess posts is single.php

But if you want a detail page of artist, you need the duplicate the single.php page and add -<name>
So in this case it will be single-artist.php
Always used the name of the registered post_type after the dash. You can find the newly registered post_type in custom-functions.php 
