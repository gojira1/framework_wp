<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package gojira
 */

?>

	</div><!-- #content -->

	<footer class="primary-footer">
		<div class="site-info container">
			<div class="row">
				<div class="col-sm-4">
					<h3>Widget</h3>
	
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis nesciunt,
						recusandae soluta dolores delectus error commodi obcaecati quis labore 
						reprehenderit at ipsum dolorem quam asperiores amet officia. Ratione maxime, quos.
					</p>
				</div>
	
				<div class="col-sm-4">
					<h3>Widget</h3>
	
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis nesciunt,
						recusandae soluta dolores delectus error commodi obcaecati quis labore 
						reprehenderit at ipsum dolorem quam asperiores amet officia. Ratione maxime, quos.
					</p>
				</div>
	
				<div class="col-sm-4">
					<h3>Widget</h3>
	
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis nesciunt,
						recusandae soluta dolores delectus error commodi obcaecati quis labore 
						reprehenderit at ipsum dolorem quam asperiores amet officia. Ratione maxime, quos.
					</p>
				</div>
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->

	<footer class="secondary-footer">
		<div class="container text--center">
			<div class="row">
			Template by <a href="http://www.gojira.be">www.gojira.be</a>
			</div>
		</div>
	</footer>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
