<?php
/**
 * Enable the thumbnails
 */

add_theme_support( 'post-thumbnails' ); 


/**
 * Create a custom post type
 */

function create_posttype() {
    register_post_type( 'artist', // All in lowercase
      array(
        'labels' => array(
          'name' => __( 'Artists' ), // Plural name of your new post type
          'singular_name' => __( 'Artist' ) // Singular name of your new post type
        ),
        'public' => true,
        'has_archive' => true,
        'rewrite' => array('slug' => 'artist'), // Singular and lowercase
      )
    );

    register_post_type( 'sponsor', // All in lowercase
      array(
        'labels' => array(
          'name' => __( 'Sponsors' ), // Plural name of your new post type
          'singular_name' => __( 'Sponsor' ) // Singular name of your new post type
        ),
        'public' => true,
        'has_archive' => true,
        'rewrite' => array('slug' => 'sponsor'), // Singular and lowercase
      )
    );

    // copy here another custom posttype
}

add_action( 'init', 'create_posttype' );


/**
 * Register our sidebars and widgetized areas.
 *
 */
function custom_widgets_init() {

  register_sidebar( array(
    'name'          => 'Footer widget left',
    'id'            => 'footer-widget--left',
    'before_widget' => '<div>',
    'after_widget'  => '</div>',
    'before_title'  => '<h2>',
    'after_title'   => '</h2>',
  ) );

  register_sidebar( array(
    'name'          => 'Footer widget center',
    'id'            => 'footer-widget--center',
    'before_widget' => '<div>',
    'after_widget'  => '</div>',
    'before_title'  => '<h2>',
    'after_title'   => '</h2>',
  ) );

  register_sidebar( array(
    'name'          => 'Footer widget right',
    'id'            => 'footer-widget--right',
    'before_widget' => '<div>',
    'after_widget'  => '</div>',
    'before_title'  => '<h2>',
    'after_title'   => '</h2>',
  ) );

}
add_action( 'widgets_init', 'custom_widgets_init' );

?>