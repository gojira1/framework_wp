<?php
/*
* Relocate/add styles
*/

function my_theme_styles() {
    wp_dequeue_style( 'gojira-style', get_stylesheet_uri() );
    wp_enqueue_style('font-lato','https://fonts.googleapis.com/css?family=Lato:400,700,300,100', false, 3 );
    wp_enqueue_style('my-theme-style', get_bloginfo('template_directory') . '/dist/assets/css/main.min.css', false, 100);
}
add_action('wp_print_styles', 'my_theme_styles');

/*
* Add scripts
*/

function my_theme_scripts() {
    wp_deregister_script( 'jquery' );
    wp_enqueue_script( 'jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js', array(), null, true );
    wp_enqueue_script( 'vendor-scripts', get_template_directory_uri() . '/dist/assets/js/vendor/vendor.min.js', array( 'jquery' ), null, true );
    wp_enqueue_script( 'custom-scripts', get_template_directory_uri() . '/dist/assets/js/app.min.js', array( 'jquery' ), null, true );
    wp_enqueue_script( 'custom-scripts' );
}
add_action( 'wp_enqueue_scripts', 'my_theme_scripts' );
?>