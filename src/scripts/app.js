$(document).ready(function(){

});

var site = {
    settings: {
        debug: false
    },
    init: function(settings){
        $.extend(this.settings, settings);
    },

    /* --------------------------
    * Create your functions here
    * --------------------------*/

    customFunction: function(parameter, parameter) {
        
    }
};

$(function(){
    site.init({
        debug: true
    });

    // smoothScroll init
    //smoothScroll.init({
    //    offset: 90
    //});

    // Load custom function
    site.customFunction();

    // Slider
    $('.slider').slick({
        infinite: true,
        speed: 300,
        slidesToShow: 2,
        slidesToScroll: 1,
        autoplay: true,
        arrows: false,
        autoplaySpeed: 2000,
        swipe: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
});
