<section class="block block__splash--tertiary subscribe text--center">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="subscribe__content">
					<h2 class="splash__title splash__title--white text--center">Subscribe</h2>
	
					<div class="splash__body splash__body--white text--center">
						<p>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit itaque.
						</p>
					</div>

					<form action="" class="newsletter__form">
                    	<input type="text" class="form-control newsletter__form__input" placeholder="Email address">
                    	<button type="submit" class="btn newsletter__form__btn">Subscribe</button>
                	</form>
				</div>
			</div>
		</div>
	</div>
</section>