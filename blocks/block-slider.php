<section class="block block__slider">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				
				<div class="slider">
					<a href="#" class="slider__item">
						<img class="slider__img" src="<?php echo get_template_directory_uri(); ?>/dist/assets/img/slider/slide.png" alt="slide" title="slide" />
					</a>
					<a href="#" class="slider__item">
						<img class="slider__img" src="<?php echo get_template_directory_uri(); ?>/dist/assets/img/slider/slide.png" alt="slide" title="slide" />
					</a>
					<a href="#" class="slider__item">
						<img class="slider__img" src="<?php echo get_template_directory_uri(); ?>/dist/assets/img/slider/slide.png" alt="slide" title="slide" />
					</a>
					<a href="#" class="slider__item">
						<img class="slider__img" src="<?php echo get_template_directory_uri(); ?>/dist/assets/img/slider/slide.png" alt="slide" title="slide" />
					</a>
					<a href="#" class="slider__item">
						<img class="slider__img" src="<?php echo get_template_directory_uri(); ?>/dist/assets/img/slider/slide.png" alt="slide" title="slide" />
					</a>
					<a href="#" class="slider__item">
						<img class="slider__img" src="<?php echo get_template_directory_uri(); ?>/dist/assets/img/slider/slide.png" alt="slide" title="slide" />
					</a>
				</div>

			</div>
		</div>
	</div>
</section>