<section class="block block__splash">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 splash__copy">
				<header>
					<h2 class="splash__title">
						This is a h2 title
					</h2>
				</header>

				<p class="splash__body">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
					Laudantium voluptate voluptatem, sint, cumque eveniet minima inventore. 
					Incidunt optio voluptatem veritatis non ab necessitatibus, odit, vero aliquid est, 
					maiores dolorum ut.
				</p>
				
				<p class="splash__cta">
					<a href="#" class="splash__link">This is a cta</a>
				</p>
			</div>

			<figure class="col-sm-6">

				<img class="splash__image" src="http://lorempixel.com/g/600/400/" />

			</figure>
		</div>
	</div>
</section>

<section class="block block__splash block__splash--grey-light block__splash--figure-left">
	<div class="container">
		<div class="row">
			<div class="col-sm-6 splash__copy">
				<header>
					<h2 class="splash__title">
						This is a h2 title
					</h2>
				</header>

				<p class="splash__body">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
					Laudantium voluptate voluptatem, sint, cumque eveniet minima inventore. 
					Incidunt optio voluptatem veritatis non ab necessitatibus, odit, vero aliquid est, 
					maiores dolorum ut.
				</p>
				
				<p class="splash__cta">
					<a href="#" class="splash__link">This is a cta</a>
				</p>
			</div>

			<figure class="col-sm-6">

				<img class="splash__image" src="http://lorempixel.com/g/600/400/" />

			</figure>
		</div>
	</div>
</section>