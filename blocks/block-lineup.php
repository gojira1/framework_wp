<section class="block__lineup">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 id="lineup">
					<span>Line-up</span>
				</h1>
			</div>

			<div class="col-md-12">
				<div class="lineup grid">
                	<div class="grid-sizer"></div>

                	<?php
                	    $args = array( 'post_type' => 'artist');
    	
                	    $loop = new WP_Query( $args );
            	
                	    while ( $loop->have_posts() ) : $loop->the_post();
                	?>

						<a href="<?php the_permalink(); ?>" class="grid-item artist">
							<div class="artist__inner-wrapper">
								<img class="artist__visual image--autosize" src="<?php the_field('artist_picture'); ?>" />
	
								<span class="artist__name">
								<?php the_title(); ?>
								</span>

							</div>
                		</a>

                	<?php
                		endwhile;
                	?>

                </div>
			</div>

			<div class="col-md-12 text--center">
				<a class="btn btn__view-all-news" href="lineup/">
					<span class="btn__text">More</span>
				</a>
			</div>

		</div>
	</div>
</section>