<section class="block__news">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 id="news">
					<span>News</span>
				</h1>
			</div>

			<div class="col-md-12">
				<div class="article-wrapper">
					<?php
                	    $args = array( 'post_type' => 'post');
    	
                	    $loop = new WP_Query( $args );
            	
                	    while ( $loop->have_posts() ) : $loop->the_post();
                	?>
                	    
           				<a href="<?php the_permalink(); ?>">
							<?php the_title(); ?>
           				</a>
    	
                	<?php
                	    endwhile;
                	?>
                </div>
			</div>

			<div class="col-md-12 text--center">
				<a class="btn btn__view-all-news" href="news/">
					<span class="btn__text">More news</span>
				</a>
			</div>
		</div>
	</div>
</section>