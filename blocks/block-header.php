<header class="container-fluid">
    <figure class="header-visual">
        <img src="<?php echo get_template_directory_uri(); ?>/dist/assets/img/header/header.png" class="header-visual__img">
        
        <figcaption class="header-visual__content">
            <div class="header-visual__content__inner">
                <h1 class="header-visual__title">
                    This is a title
                </h1>

                <p class="header-visual__text">
                    Lorem ipsum dolor sit amet iam consectetur placeat harum explicabo.
                </p>
        
                <a href="/en/services" class="btn header-visual__btn">
                    Our services
                </a>
            </div>
        </figcaption>
    </figure>
</header>