<section class="block__sponsors">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1 id="news">
					<span>News</span>
				</h1>
			</div>

			<div class="col-md-12">
				<div class="sponsor-wrapper">
					<?php
                	    $args = array( 'post_type' => 'sponsor');
    	
                	    $loop = new WP_Query( $args );
            	
                	    while ( $loop->have_posts() ) : $loop->the_post();
                	?>
                	    
           				<a href="<?php the_permalink(); ?>">
							<?php the_title(); ?>
           				</a>
    	
                	<?php
                	    endwhile;
                	?>
                </div>
			</div>

		</div>
	</div>
</section>